Node.js Developer Coding Test

## Introduction

Your task is to create a RESTful API using Node.js that manages and executes batch jobs. The API should allow users to create, monitor, and stop batch jobs.

## Requirements

### Technology Stack

- Node.js
- Express.js

### Features

1. **Create a Batch Job**
    - Endpoint: `POST /batch-jobs`
    - Input: A JSON object containing a `duration` field (in milliseconds) which indicates how long the batch job will run.
    - Action: Start a simulated batch job using `setTimeout` for the specified duration.
    - Response: Return a unique job ID for the created batch job.

2. **Get the Status of All Batch Jobs**
    - Endpoint: `GET /batch-jobs`
    - Action: Retrieve the status of all batch jobs.
    - Response: A list of batch jobs with their IDs, status (e.g., "running", "completed", "stopped"), and duration.

3. **Get the Status of an Individual Batch Job**
    - Endpoint: `GET /batch-jobs/:jobId`
    - Action: Retrieve the status of a specific batch job using its ID.
    - Response: Details of the batch job including its ID, status, and duration.

4. **Stop an Individual Batch Job**
    - Endpoint: `DELETE /batch-jobs/:jobId`
    - Action: Stop a running batch job using its ID. If the job is already completed or stopped, return an appropriate message.
    - Response: A message indicating the result of the action (e.g., "Job stopped successfully", "Job already completed").

### Bonus

1. **Create a Batch Job with Dependencies**
    - Input: A JSON object containing:
        - `duration`: How long the batch job will run.
        - `dependencies`: An array of job IDs that this job depends on.
    - Action: Ensure jobs with dependencies complete before starting this job.

2. **Logging Mechanism**
    - Implement a logging mechanism that logs every job's start, completion, and stop events.
    - Endpoint: `GET /batch-jobs/:jobId/logs`
    - Action: Retrieve the logs associated with a specific batch job.
    - Response: A list of log entries for the specified batch job.

3. Implement error handling for scenarios like invalid job IDs, circular dependencies, etc.

## Evaluation Criteria

- **Functionality**: Does the API work as expected?
- **Concurrency Handling**: How well does the application handle multiple batch jobs and their statuses?
- **Code Quality**: Is the code clean, modular, and easy to understand?
- **Error Handling**: How does the application handle potential errors or unexpected inputs?
const express = require('express');
const app = express();
const PORT = 4000;

// Middleware to parse JSON payloads
app.use(express.json());

app.get('/', (req, res) => {
  res.send('Hello, World!');
});

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
